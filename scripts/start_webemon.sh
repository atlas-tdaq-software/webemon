#!/bin/bash

LOGFILE=/logs/$1/web_is_ext/log.web_is_ext.$HOSTNAME
if [ -z "$TDAQ_INST_PATH" ]; then
    # Only at P1 we have a 'standard' release
    if [ -f /det/tdaq/scripts/setup_TDAQ.sh ]; then
        source /det/tdaq/scripts/setup_TDAQ_$1.sh
    else
        echo "TDAQ release is not set up"
        exit 1
    fi
fi

if [ -d `pwd`/external ]; then
   export PYTHONPATH=`pwd`/external:${PYTHONPATH}
fi

if [ -d `pwd`/installed ]; then
    export PYTHONPATH=`pwd`/installed/${CMTCONFIG}/lib:${PYTHONPATH}
    export LD_LIBRARY_PATH=`pwd`/installed/${CMTCONFIG}/lib:${LD_LIBRARY_PATH}
fi

unset DISPLAY
while true
do
    webemon_server.py &> ${LOGFILE} &
done

