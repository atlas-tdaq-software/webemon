#!/usr/bin/env tdaq_python

import argparse

import sys, DLFCN
sys.setdlopenflags(DLFCN.RTLD_NOW | DLFCN.RTLD_GLOBAL)

import ipc
import ers
import emon_helper

xsl_default  = '<?xml-stylesheet type="text/xsl" href="/tdaq/web_is/xsl/default.xsl"?>'

def get_releases(environ, start_response):
    """
    Get list of valid releases. This is static for now.
    """
    start_response('200 OK', [('Content-Type','application/xml')])

    return ['''<?xml version="1.0" encoding="UTF-8"?>
%s
<releases>
<release name="current"/>
</releases>
    ''' % xsl_default ]

def get_partitions(environ, start_response, release):
    """
    Get list of active partitions.
    """
    partitions = [ p.name() for p in ipc.getPartitions() ]
    partitions.append('initial')
    partitions.sort(key=str.lower)
    return [emon_helper.getPartitions()]
    
def get_emon_samplers(partition, start_response):
    partition = ipc.IPCPartition(partition)
    return [emon_helper.getSamplers(partition)]

def get_emon_event(partition, sampler_type, sampler_name, criteria, start_response):
    return [emon_helper.getEvent(partition, sampler_type, sampler_name, criteria)]    
    
def emon_app(environ, start_response):    
    path = environ['PATH_INFO']
    attr = environ['QUERY_STRING']
        
    ers.log('Received "' + path + '" request with the "' + attr + '" attributes')
    
    args = path.split('/')
    if len(args) == 3 or len(args) > 4:
        start_response("404 Not found", [('Content-Type','text/plain')])
        return [ "Invalid request path: " + path]

    try:
        result = ''
        if len(args) == 1:
            result = get_partitions(environ, start_response, release)
        elif len(args) == 2:
            result = get_emon_samplers(args[1], start_response)
        else:
            criteria = dict([a.split('=') for a in attr.split('&')])
            result = get_emon_event(args[1], args[2], args[3], criteria, start_response)
        
        start_response('200 OK', [('Content-Type','application/xml')])
        return result
    except Exception as ex:
        ers.log(ex)
        start_response("404 Not found", [('Content-Type','text/plain')])
        return str(ex)
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Start Web EMON service.')
    parser.add_argument('-p', '--port', type=int, default=4080, help='port to listen')

    args = parser.parse_args()

    from wsgiref.simple_server import make_server
    httpd = make_server('', args.port, emon_app)
    ers.log('Web EMON service is started on port %d' % args.port)
    httpd.serve_forever()
