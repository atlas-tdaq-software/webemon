#!/usr/bin/env tdaq_python

import eformat
import eformat2xml

import sys, DLFCN
sys.setdlopenflags(DLFCN.RTLD_NOW | DLFCN.RTLD_GLOBAL)

import ipc
import emon
import ers

import xml.dom.minidom

def partition2xml(doc, node, partition, add_samplers):
    p_entry = doc.createElement('partition')
    p_entry.setAttribute("name", partition.name())
    node.appendChild(p_entry)
    if add_samplers:
        samplers2xml(doc, p_entry)

def samplers2xml(doc, node):
    try:
        samplers = emon.getEventSamplers(ipc.IPCPartition(node.getAttribute("name")))
        tree = {}
        for sampler in samplers:
            pair = sampler.split(':', 1)
            if pair[0] in tree:
                tree[pair[0]].append(pair[1])
            else:
                t_entry = doc.createElement('sampler')
                t_entry.setAttribute("type", pair[0])
                tree[pair[0]] = [pair[1]]
                
        for type, names in tree.items():
            t_entry = doc.createElement('samplers')
            t_entry.setAttribute("type", type)
            s_node = doc.createTextNode(' '.join(n for n in names))
            t_entry.appendChild(s_node)
            node.appendChild(t_entry)
    except ers.Issue as ex:
        ers.log(ex)

def partitions2xml(p, add_samplers):
    doc = xml.dom.minidom.Document()
    node = doc.createElement('partitions')
    doc.appendChild(node)
    for partition in [p] if p <> None else ipc.getPartitions():
        partition2xml(doc, node, partition, add_samplers)
    return (doc.toprettyxml())
    
def getPartitions():
    return partitions2xml(None, False)
    
def getSamplers(partition):
    return partitions2xml(partition, True)

def getAllSamplers():
    return partitions2xml(None, True)
        
def getEvent(part, type, name, criteria):
    partition = ipc.IPCPartition(part)
    address = emon.SamplingAddress(type, name)
    try:
        with emon.EventIterator(partition, address, parseSelectionCriteria(criteria)) as iterator:
            event = iterator.nextEvent(10000)
            return eformat2xml.eventToXML(eformat.FullEventFragment(event))
    except ers.Issue as ex:
        ers.log(ex)
        raise

def parseSelectionCriteria(criteria):
    if "l1_trigger_type" in criteria:
         L1_Trigger_Type = emon.L1TriggerType(int(criteria.get("l1_trigger_type")), False)
    else:
         L1_Trigger_Type = emon.L1TriggerType(0, True)

    if "status" in criteria:
         Status_Word = emon.StatusWord(int(criteria.get("status")), False)
    else:
         Status_Word = emon.StatusWord(0, True)

    if ("stream_tags" in criteria):
        streams = criteria.get("stream_tags").split('+')
        Stream_Tags = emon.StreamTags(streams[0] if len(streams) > 0 else '', 
                                streams[1:] if len(streams) > 1 else [], 
                                emon.Logic.OR if len(streams) > 1 else emon.Logic.IGNORE)
    else:
        Stream_Tags = emon.StreamTags('',[],emon.Logic.IGNORE)

    if ("l1_trigger_bits" in criteria):
        bits = [int(b) for b in criteria.get("l1_trigger_bits").split("+")]
        L1_Trigger_Bits = emon.L1TriggerBits(bits, emon.Logic.OR, emon.Origin.AFTER_VETO)
    else:
        L1_Trigger_Bits = emon.L1TriggerBits([], emon.Logic.IGNORE, emon.Origin.AFTER_VETO)
        
    return emon.SelectionCriteria(L1_Trigger_Type, L1_Trigger_Bits, Stream_Tags, Status_Word)

if __name__ == '__main__':
   print "get partition list:"
   print getPartitions()

   print "get samplers with partitions"
   print getAllSamplers()

   print "get an event"
   print getEvent('EmonTest','EFD','EFD-1',[])
