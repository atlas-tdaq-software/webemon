"""WEB extension for Event monitoring service

This module is part of the WEB extension for the Event Monitoring Service
package of the ATLAS TDAQ system.
"""

__author__ = "Serguei Kolos (Serguei.Kolos@cern.ch)"
__version__ = "$Revision: 1.0 $"
__date__ = "$Date: 2013/06/05 21:57:20 $"
__all__ = ["eformat2xml", "emon_helper"]
