import xml.dom.minidom
import datetime

from xml.dom.minicompat import *
from xml.dom import EMPTY_NAMESPACE
from xml.dom.minidom import DOMImplementation
from collections import OrderedDict

import eformat

def stringFromArray(array):
    return ' '.join('0x%08x' % e for e in array) 

def versionConverter(a):
    b = a.split("-")
    return b[0] + "." + b[1]

def insertCheckSum(type, value, doc, entry):
    if type == 0x1:
        stype = "CRC-16"
    elif type == 0x2:
        stype = "Adler-32"
    else:
        return
    c_entry = doc.createElement('checksum')
    c_entry.setAttribute("type",stype)
    c_entry.appendChild(doc.createTextNode(str(value)))
    entry.appendChild(c_entry)
        
def insertStreamTags(value, doc, entry):        
    streams = doc.createElement('event_stream')
    for v in value:
        tag = doc.createElement('tag')
        tag.setAttribute("type", v.type)
        tag.setAttribute("name", v.name)
        tag.setAttribute("obeys_lumiblock", str(v.obeys_lumiblock))
        streams.appendChild(tag)
    entry.appendChild(streams)
        
# Changes in XML description to prevent alphabetical sorting

class list_without_sort(list):
    def sort(self): pass

class NamedNodeMap(xml.dom.minidom.NamedNodeMap):
    def keys(self):
        return list_without_sort(xml.dom.minidom.NamedNodeMap.keys(self))

class Element(xml.dom.minidom.Element):
    def _get_attributes(self):
        return NamedNodeMap(self._attrs, self._attrsNS, self)

    def __init__(self, tagName, namespaceURI=EMPTY_NAMESPACE, prefix=None,localName=None):
        self.tagName = self.nodeName = tagName
        self.prefix = prefix
        self.namespaceURI = namespaceURI
        self.childNodes = NodeList()

        self._attrs = OrderedDict()
        self._attrsNS = {}

class Document(xml.dom.minidom.Document):
    def createElement(self, tagName):
        e = Element(tagName)
        e.ownerDocument = self
        return e

# end of Changes in XML description to prevent alphabetical sorting

def eventToXML(e):

    imp = DOMImplementation()
    doctype = imp.createDocumentType(
        qualifiedName='event',
        publicId='',
        systemId='https://atlasop.cern.ch/tdaq/web_emon/dtd/event.dtd',
    )
    doc = imp.createDocument(None, 'event', doctype)    

    base = doc.documentElement
    doc.appendChild(base)

    base.setAttribute('source',str(e.source_id()))
    base.setAttribute('run_type',str(e.run_type()))
    base.setAttribute('run_number',str(e.run_no()))

    base.setAttribute('lumi_block',str(e.lumi_block()))
    base.setAttribute('global_id',str(e.global_id()))

    base.setAttribute('bcid',str(e.bc_id()))

    ts = datetime.datetime.fromtimestamp(e.bc_time_seconds()).strftime('%Y-%m-%d %H:%M:%S')
    if e.bc_time_nanoseconds()==0:
        base.setAttribute('bc_time',ts)
    else:
        base.setAttribute('bc_time',ts+"."+str(e.bc_time_nanoseconds()).zfill(9))

    base.setAttribute('l1_id',str(e.lvl1_id()))
    base.setAttribute('l1_trigger_type',str(e.lvl1_trigger_type()))

    base.setAttribute('version',versionConverter(str(e.version())))

    insertCheckSum(e.checksum_type(), e.checksum_value(), doc, base)

    insertStreamTags(e.stream_tag(), doc, base)

    # Full Event status elements
    e_status = e.status()
    if (len(e_status)>0):
        e_status_entry = doc.createElement('event_status')
        e_status_content = doc.createTextNode(stringFromArray(e_status))
        e_status_entry.appendChild(e_status_content)
        base.appendChild(e_status_entry)

    parameter = stringFromArray(e.event_filter_info())
    if parameter == "[]" or parameter=="":
        pass
    else:
        e_ef_info_entry = doc.createElement('ef_info')
        e_ef_info_content = doc.createTextNode(parameter)
        e_ef_info_entry.appendChild(e_ef_info_content)
        base.appendChild(e_ef_info_entry)

    parameter = stringFromArray(e.lvl1_trigger_info())
    if parameter == "[]" or parameter=="":
        pass
    else:
        e_l1_trigger_info_entry = doc.createElement('l1_info')
        e_l1_trigger_info_content = doc.createTextNode(parameter)
        e_l1_trigger_info_entry.appendChild(e_l1_trigger_info_content)
        base.appendChild(e_l1_trigger_info_entry)

    parameter = stringFromArray(e.lvl2_trigger_info())
    if parameter ==  "[]" or parameter=="":
        pass
    else:
        e_l2_trigger_info_entry = doc.createElement('l2_info')
        e_l2_trigger_info_content = doc.createTextNode(parameter)
        e_l2_trigger_info_entry.appendChild(e_l2_trigger_info_content)
        base.appendChild(e_l2_trigger_info_entry)


    # end of FULL EVENT reconstruction

    h=e.header
    hm=hex(e.marker())
    hs=e.header_size_word()
    rob_iterator = e.children()

    try:
        while True:
            try:
                rob = rob_iterator.next()
                rob_entry = doc.createElement('rob')
                base.appendChild(rob_entry)

                rob_entry.setAttribute("source",str(rob.rob_source_id()))
                rob_entry.setAttribute("version",versionConverter(str(rob.version())))

                insertCheckSum(rob.checksum_type(), rob.checksum_value(), doc, rob_entry)

                rb_status = rob.status()
                if (len(rb_status)>0):
                     rob_status_entry = doc.createElement('status')
                     rob_status_content = doc.createTextNode(stringFromArray(rb_status))
                     rob_status_entry.appendChild(rob_status_content)
                     rob_entry.appendChild(rob_status_entry)

                rod_entry = doc.createElement('rod')
                rob_entry.appendChild(rod_entry)

                rod_entry.setAttribute("source",str(rob.rod_source_id()))
                rod_entry.setAttribute("run_number",str(rob.rod_run_no()))
                rod_entry.setAttribute("l1_id",str(rob.rod_lvl1_id()))
                rod_entry.setAttribute("bcid",str(rob.rod_bc_id()))
                rod_entry.setAttribute("l1_trigger_type",str(rob.rod_lvl1_trigger_type()))
                rod_entry.setAttribute("det_event_type",str(rob.rod_detev_type()))
                rod_entry.setAttribute("version",versionConverter(str(rob.rod_version())))

                r_status = rob.rod_status()
                if (len(r_status)>0):
                     rod_status_entry = doc.createElement('status')
                     rod_status_content = doc.createTextNode(stringFromArray(r_status))
                     rod_status_entry.appendChild(rod_status_content)
                     rod_entry.appendChild(rod_status_entry)

                r_data = rob.rod_data()
                if (len(r_data)>0):
                     rod_data_entry = doc.createElement('data')
                     rod_data_content = doc.createTextNode(stringFromArray(r_data))
                     rod_data_entry.appendChild(rod_data_content)
                     rod_entry.appendChild(rod_data_entry)

            except RuntimeError:
                pass
            else:
                pass
    except StopIteration:
        pass

    return doc.toprettyxml()
 
