Run server:
start_webemon.sh

Check status:
./webemon status

Stop server:
./webemon stop

HTTP requests:
http://host_name.cern.ch:8000/web_emon/test
http://host_name.cern.ch:8000/web_emon/test/DCM/DCM-1

Where:
test - partition name
DCM - emon sampler type
DCM-1 - emon sampler name

Setup of the test partition (test) with 2 samplers:

ipc_server &
ipc_server -p test &
emon_conductor -p test &
emon_push_sampler -p test -t DCM -n DCM-1 -F $TDAQ_INST_PATH/../ed/data/EVENTv4.dat &
emon_push_sampler -p test -t ROS -n ROS-A -F $TDAQ_INST_PATH/../ed/data/EVENTv4.dat &
